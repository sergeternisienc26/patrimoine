<?php

namespace App\Form;

use App\Entity\Maintenance;
use App\Entity\Patrimony;
use App\Entity\Types;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class PatrimonyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name')
            ->add('description')
            ->add('works')
            ->add('createdAt')
            ->add('lat')
            ->add('lon')
            ->add('thumbnail', FileType::class, [
                'label' => 'vignette patrimoine',
                'required' => false,
                'data_class' => NULL,
                'mapped' => false,
                'constraints' => [
                    new File([
                        'maxSize' => '6M',
                        'mimeTypes' => [
                            'image/*',
                        ],
                        'mimeTypesMessage' => 'Le fichier n\'est pas valide, assurez vous d\'avoir un fichier au format PNG, JPG, JPEG)',
                    ]),
                ]
            ])

            ->add('maintenance', EntityType::class, [
                'class' => Maintenance::class,
                'choice_label' => 'name',
                'expanded' => true,
            ])
            ->add('type', EntityType::class, [
                'class' => Types::class,
                'choice_label' => 'name',
                'expanded' => true,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Patrimony::class,
        ]);
    }
}
