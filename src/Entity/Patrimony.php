<?php

namespace App\Entity;

use App\Repository\PatrimonyRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=PatrimonyRepository::class)
 */
class Patrimony
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"patrimonyJson"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"patrimonyJson"})
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     * @Groups({"patrimonyJson"})
     */
    private $description;

    /**
     * @ORM\Column(type="text")
     * @Groups({"patrimonyJson"})
     */
    private $works;

    /**
     * @ORM\Column(type="datetime_immutable")
     * @Groups({"patrimonyJson"})
     */
    private $createdAt;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"patrimonyJson"})
     */
    private $thumbnail;

    /**
     * @ORM\ManyToOne(targetEntity=Maintenance::class, inversedBy="patrimonies")
     */
    private $maintenance;

    /**
     * @ORM\ManyToOne(targetEntity=Types::class, inversedBy="patrimonies")
     */
    private $type;

    /**
     * @ORM\OneToMany(targetEntity=Comment::class, mappedBy="patrimony", cascade={"persist", "remove"})
     */
    private $comments;

    /**
     * @ORM\OneToMany(targetEntity=Pictures::class, mappedBy="patrimony", cascade={"persist", "remove"})
     */
    private $pictures;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Groups({"patrimonyJson"})
     */
    private $lat;

    /**
     * @ORM\Column(type="float", nullable=true)   
     * @Groups({"patrimonyJson"})
     */
    private $lon;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $dateMaintenance;

    public function __construct()
    {
        $this->comments = new ArrayCollection();
        $this->pictures = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getWorks(): ?string
    {
        return $this->works;
    }

    public function setWorks(string $works): self
    {
        $this->works = $works;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getThumbnail(): ?string
    {
        return $this->thumbnail;
    }

    public function setThumbnail(string $thumbnail): self
    {
        $this->thumbnail = $thumbnail;

        return $this;
    }

    public function getMaintenance(): ?Maintenance
    {
        return $this->maintenance;
    }

    public function setMaintenance(?Maintenance $maintenance): self
    {
        $this->maintenance = $maintenance;

        return $this;
    }

    public function getType(): ?Types
    {
        return $this->type;
    }

    public function setType(?Types $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return Collection|Comment[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setPatrimony($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->removeElement($comment)) {
            // set the owning side to null (unless already changed)
            if ($comment->getPatrimony() === $this) {
                $comment->setPatrimony(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Pictures[]
     */
    public function getPictures(): Collection
    {
        return $this->pictures;
    }

    public function addPicture(Pictures $picture): self
    {
        if (!$this->pictures->contains($picture)) {
            $this->pictures[] = $picture;
            $picture->setPatrimony($this);
        }

        return $this;
    }

    public function removePicture(Pictures $picture): self
    {
        if ($this->pictures->removeElement($picture)) {
            // set the owning side to null (unless already changed)
            if ($picture->getPatrimony() === $this) {
                $picture->setPatrimony(null);
            }
        }

        return $this;
    }

    public function getLat(): ?float
    {
        return $this->lat;
    }

    public function setLat(?float $lat): self
    {
        $this->lat = $lat;

        return $this;
    }

    public function getLon(): ?float
    {
        return $this->lon;
    }

    public function setLon(?float $lon): self
    {
        $this->lon = $lon;

        return $this;
    }

    public function getDateMaintenance(): ?\DateTimeInterface
    {
        return $this->dateMaintenance;
    }

    public function setDateMaintenance(?\DateTimeInterface $dateMaintenance): self
    {
        $this->dateMaintenance = $dateMaintenance;

        return $this;
    }
}
