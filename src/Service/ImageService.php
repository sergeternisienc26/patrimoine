<?php

namespace App\Service;

use App\Controller\PatrimonyController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class ImageService extends PatrimonyController
{

    public function getInsertImage($imagePatrimony, $slugger, $route): string

    {
            $originalFilename = pathinfo($imagePatrimony->getClientOriginalName(), PATHINFO_FILENAME);
            // ceci est nécessaire pour inclure en toute sécurité le nom du fichier dans l'URL
            $safeFilename = $slugger->slug($originalFilename);
            $newFilename = $safeFilename . '-' . uniqid() . '.' . $imagePatrimony->guessExtension();

            // Déplacer le fichier dans le répertoire où sont stockées les images
            try {
                $imagePatrimony->move(
                    $this->getParameter($route),
                    $newFilename
                );
            } catch (FileException $e) {
                // gérer l'exception si quelque chose se produit pendant le téléchargement du fichier
            }
        

        return $newFilename;
    }
}
