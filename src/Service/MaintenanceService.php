<?php

namespace App\Service;

use DateTime;

class MaintenanceService
{
    public function getMaintenance($allArrayPatrimony): array
    {
        //======================================
        //  gestion cnt periodicité maintenance
        //======================================

        $mensuelValueSec = 2592000; //en seconde correspond à mois

        //create date du jour puis convert en format timestamp unix
        $dateNow = new DateTime('NOW');
        $dateNowUnix = $dateNow->format('U');

        //boucle sur tableau patrimony
        foreach ($allArrayPatrimony as $ObjectPatrimony) {
            //prop periodicity maintenance
            $maintPeriodicity = $ObjectPatrimony->getMaintenance()->getPeriodicity();
            //date création patrimoine //convert en timestramp unix
            $dateCreatePatrimony = ($ObjectPatrimony->getCreatedAt())->format('U');
            //affectation valeur sur périodicity en sec
            switch ($maintPeriodicity) {
                case 'mensuel':
                    $periodicityValue = $mensuelValueSec;
                    break;
                case '6mensuel':
                    $periodicityValue = $mensuelValueSec * 6;
                    break;
                case '12mensuel':
                    $periodicityValue = $mensuelValueSec * 12;
                    break;
                case '24mensuel':
                    $periodicityValue = $mensuelValueSec * 24;
                    break;
                case '48mensuel':
                    $periodicityValue = $mensuelValueSec * 48;
                    break;
                default:
                    echo "aucune condition remplie";
                    break;
            }

            //create prop. alert for ObjectPatrimony
            $ObjectPatrimony->alert = "red";
            //cnt déclenchement alert
            if (($periodicityValue + $dateCreatePatrimony + (6 * $mensuelValueSec)) < $dateNowUnix) {
                $ObjectPatrimony->alert = "red";
            } elseif (($periodicityValue + $dateCreatePatrimony) <  $dateNowUnix) {
                $ObjectPatrimony->alert = "orange";
            } else {
                $ObjectPatrimony->alert = "green";
            }
        }
    return $allArrayPatrimony;

    }

}
