<?php

namespace App\Controller;

use App\Repository\PatrimonyRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class HomeController extends AbstractController
{
    #[Route('/', name: 'home')]
    public function index(PatrimonyRepository $patrimonyRepository, SerializerInterface $serializer): Response
    {
        $allArrayPatrimony = $patrimonyRepository->findAll();
        $jsonPatrimony = $serializer->serialize($allArrayPatrimony, 'json', ['groups' => 'patrimonyJson']);

        return $this->render('home/index.html.twig', [
            'patrimonies' =>$allArrayPatrimony,
            'jsonPatrimony' =>$jsonPatrimony,
        ]);
    }
}
