<?php

namespace App\Controller;

use DateTime;
use DateTimeImmutable;
use App\Entity\Comment;
use App\Entity\Pictures;
use App\Entity\Patrimony;
use App\Form\CommentType;
use App\Form\PicturesType;
use App\Form\PatrimonyType;
use App\Service\MaintenanceService;
use App\Repository\CommentRepository;
use App\Repository\PatrimonyRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\MaintenanceRepository;
use App\Service\ImageService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

#[Route('/patrimony')]
class PatrimonyController extends AbstractController
{
    #[Route('/', name: 'patrimony_index', methods: ['GET', 'POST']), IsGranted('ROLE_ADMIN')]
    public function index(PatrimonyRepository $patrimonyRepository, MaintenanceRepository $maintenanceRepository, MaintenanceService $maintenanceService): Response
    {
        //tableau de ttes les prop. patrimony
        $allArrayPatrimony = $patrimonyRepository->findAll();
        //tableau de ttes les prop. maintenance
        $allArrayMaintenance = $maintenanceRepository->findAll();

        $maintenance = $maintenanceService->getMaintenance($allArrayPatrimony);

        return $this->render('patrimony/index.html.twig', [
            'patrimonies' =>  $maintenance,
            'maintenances' => $allArrayMaintenance,

        ]);
    }

    #[Route('/new', name: 'patrimony_new', methods: ['GET', 'POST']), IsGranted('ROLE_USER')]
    public function new(Request $request, EntityManagerInterface $entityManager, SluggerInterface $slugger, ImageService $imageService): Response
    {
        $patrimony = new Patrimony();
        $patrimony->setCreatedAt(new DateTimeImmutable('now'));
        $formPatrimony = $this->createForm(PatrimonyType::class, $patrimony);
        $formPatrimony->handleRequest($request);

        if ($formPatrimony->isSubmitted() && $formPatrimony->isValid()) {

            /** @var UploadedFile $imageType */
            $imagePatrimony = $formPatrimony->get('thumbnail')->getData();

            // cette condition est nécessaire car le champ 'imageType' n'est pas obligatoire
            // donc le fichier PDF/JPG/PNG doit être traité uniquement lorsqu'un fichier est téléchargé
            if ($imagePatrimony) {
              
                $image = $imageService->getInsertImage($imagePatrimony, $slugger, 'images_directory_patrimony_thumbnail');

                // met à jour la propriété 'imageTypename' pour stocker le nom du fichier PDF/JPG/PNG
                // au lieu de son contenu
                $patrimony->setThumbnail($image);
            }

            $entityManager->persist($patrimony);
            $entityManager->flush();

            return $this->redirectToRoute('home', [], Response::HTTP_SEE_OTHER);
        }

        $latitude = isset($_GET["lat"]) ? htmlspecialchars($_GET["lat"]) : "";
        $longitude = isset($_GET["lng"]) ? htmlspecialchars($_GET["lng"]) : "";

        return $this->renderForm('patrimony/new.html.twig', [
            'patrimony' => $patrimony,
            'formPatrimony' => $formPatrimony,
            'latitude' => $latitude,
            'longitude' => $longitude,
        ]);
    }

    #[Route('/show/{id}', name: 'patrimony_show', methods: ['GET', 'POST'])]
    public function show(Patrimony $patrimony, Request $request, EntityManagerInterface $entityManager, SluggerInterface $slugger, ImageService $imageService): Response
    {
        $user = $this->getUser();
        $comment = new Comment();
        $patrimonyId = $patrimony->getId();
        $formComment = $this->createForm(CommentType::class, $comment);
        $formComment->handleRequest($request);

        if ($formComment->isSubmitted() && $formComment->isValid()) {
            
            $comment->setCreatedAt(new \DateTimeImmutable());
            $comment->setPatrimony($patrimony);
            // $comment->setUser($user);

            $entityManager->persist($comment);
            $entityManager->flush();

            $this->addFlash('message', 'Votre commentaire a bien été envoyé');

            return $this->redirectToRoute('patrimony_show', ["id" => $patrimonyId], Response::HTTP_SEE_OTHER);
        }

        $pictures = new Pictures();
        $patrimonyId = $patrimony->getId();
        $formPictures = $this->createForm(PicturesType::class, $pictures);
        $formPictures->handleRequest($request);

        if ($formPictures->isSubmitted() && $formPictures->isValid()) {
            $pictures->setPatrimony($patrimony);
            // $comment->setUser($user);

            /** @var UploadedFile $imageType */
            $picturesPatrimony = $formPictures->get('photo')->getData();

            // cette condition est nécessaire car le champ 'imageType' n'est pas obligatoire
            // donc le fichier PDF/JPG/PNG doit être traité uniquement lorsqu'un fichier est téléchargé
            if ($picturesPatrimony) {

                $image = $imageService->getInsertImage($picturesPatrimony, $slugger, 'images_directory_patrimony_pictures');

                // met à jour la propriété 'imageTypename' pour stocker le nom du fichier PDF/JPG/PNG
                // au lieu de son contenu
                $pictures->setPhoto($image);
            }

            $entityManager->persist($pictures);
            $entityManager->flush();

            return $this->redirectToRoute('patrimony_show', ["id" => $patrimonyId], Response::HTTP_SEE_OTHER);
        }

        return $this->render('patrimony/show.html.twig', [
            'patrimony' => $patrimony,
            'commentForm' => $formComment->createView(),
            'picturesForm' => $formPictures->createView(),
        ]);
    }

    #[Route('/edit/{id}', name: 'patrimony_edit', methods: ['GET', 'POST']), IsGranted('ROLE_ADMIN')]
    public function edit(Request $request, Patrimony $patrimony, EntityManagerInterface $entityManager, SluggerInterface $slugger, ImageService $imageService): Response
    {
        $formPatrimony = $this->createForm(PatrimonyType::class, $patrimony);
        $formPatrimony->handleRequest($request);

        if ($formPatrimony->isSubmitted() && $formPatrimony->isValid()) {

            /** @var UploadedFile $imageType */
            $imagePatrimony = $formPatrimony->get('thumbnail')->getData();

            // cette condition est nécessaire car le champ 'imageType' n'est pas obligatoire
            // donc le fichier PDF/JPG/PNG doit être traité uniquement lorsqu'un fichier est téléchargé
            if ($imagePatrimony) {

                $image = $imageService->getInsertImage($imagePatrimony, $slugger, 'images_directory_patrimony_thumbnail');
                // met à jour la propriété 'imageTypename' pour stocker le nom du fichier PDF/JPG/PNG
                // au lieu de son contenu
                $patrimony->setThumbnail($image);
            }

            $entityManager->flush();

            return $this->redirectToRoute('patrimony_index', [], Response::HTTP_SEE_OTHER);
        }

        $latitude = isset($_GET["lat"]) ? htmlspecialchars($_GET["lat"]) : "";
        $longitude = isset($_GET["lng"]) ? htmlspecialchars($_GET["lng"]) : "";

        return $this->renderForm('patrimony/edit.html.twig', [
            'patrimony' => $patrimony,
            'formPatrimony' => $formPatrimony,
            'latitude' => $latitude,
            'longitude' => $longitude,
        ]);
    }

    #[Route('/delete/{id}', name: 'patrimony_delete', methods: ['POST']), IsGranted('ROLE_ADMIN')]
    public function delete(Request $request, Patrimony $patrimony, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete' . $patrimony->getId(), $request->request->get('_token'))) {
            $entityManager->remove($patrimony);
            $entityManager->flush();
        }

        return $this->redirectToRoute('patrimony_index', [], Response::HTTP_SEE_OTHER);
    }
}
