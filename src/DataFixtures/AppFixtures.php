<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\Entity\Types;
use App\Entity\Comment;
use App\Entity\Pictures;
use App\Entity\Patrimony;
use App\Entity\Maintenance;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{

    public function __construct(UserPasswordHasherInterface $hash) //
    {
        $this->hasher = $hash;
    }

    public function load(ObjectManager $manager): void
    {
        $date = new \DateTimeImmutable('now');

        // create 5 types! 
        {   $type = new Types();
            $type->setName('Architectural');
            $type->setDescription('architecture');
            $manager->persist($type);
        }
        {   $type = new Types();
            $type->setName('Paysager');
            $type->setDescription('paysage chemins');
            $manager->persist($type);
        }
        {   $type = new Types();
            $type->setName('Culturel');
            $type->setDescription('culturel');
            $manager->persist($type);
        }

        // create 5 maintenances! 
        {
            $maintenance = new Maintenance();
            $maintenance->setName('maintenance01');
            $maintenance->setDescription('Divers01');
            $maintenance->setPeriodicity('mensuel');
            $manager->persist($maintenance);
        }
        {
            $maintenance = new Maintenance();
            $maintenance->setName('maintenance02');
            $maintenance->setDescription('Divers02');
            $maintenance->setPeriodicity('6mensuel');
            $manager->persist($maintenance);
        }
        {
            $maintenance = new Maintenance();
            $maintenance->setName('maintenance03');
            $maintenance->setDescription('Divers03');
            $maintenance->setPeriodicity('12mensuel');
            $manager->persist($maintenance);
        }
        {
            $maintenance = new Maintenance();
            $maintenance->setName('maintenance04');
            $maintenance->setDescription('Divers04');
            $maintenance->setPeriodicity('24mensuel');
            $manager->persist($maintenance);
        }
        {
            $maintenance = new Maintenance();
            $maintenance->setName('maintenance05');
            $maintenance->setDescription('Divers05');
            $maintenance->setPeriodicity('48mensuel');
            $manager->persist($maintenance);
        }

        // create 10 patrimoine! 
        for ($i = 0; $i < 5; $i++) {
            $patrimoine = new Patrimony(); //instance classe
            $patrimoine->setName('fontaine ' . $i);
            $patrimoine->setDescription('merveilleux' . $i);
            $patrimoine->setLat(44.99  . $i);
            $patrimoine->setLon(3.68 . $i);
            $patrimoine->setWorks('beaucoup' . $i);
            $patrimoine->setCreatedAt($date);
            $patrimoine->setThumbnail('vignette' . $i);
            $patrimoine->setMaintenance($maintenance);
            $patrimoine->settype($type);
            $manager->persist($patrimoine);
        }

        // create 5 users! 
        for ($i = 0; $i < 5; $i++) {
            $user = new User();
            $user->setNickname('user' . $i);
            $password = $this->hasher->hashPassword($user, 'user');
            $user->setPassword($password);
            $user->setEmail(mt_rand(50, 100) . 'az' . $i . '@user.com');
            $user->setRoles(["ROLE_USER"]);
            $manager->persist($user);
        }

        //create admin
        $user = new User();
        $user->setNickname('admin' . $i);
        $password = $this->hasher->hashPassword($user, 'admin');
        // $user->setPassword(
        //     $this->passwordEncoder->encodePassword(
        //         $user,
        //         'test'
        //     )
        // );
        $user->setPassword($password);
        $user->setEmail(mt_rand(50, 100) . 'az' . $i . '@admin.com');
        $user->setRoles(["ROLE_ADMIN"]);
        $manager->persist($user);

        // create 10 comments! 
        for ($i = 0; $i < 10; $i++) {
            $comment = new Comment();
            $comment->setContent('superbe magnifique ' . $i);
            $comment->setCreatedAt($date);
            $comment->setUser($user);
            $comment->setPatrimony($patrimoine);
            $manager->persist($comment);
        }

        // create 10 pictures! 
        for ($i = 0; $i < 10; $i++) {
            $pictures = new Pictures();
            $pictures->setPhoto('photo' . $i);
            $pictures->setLegend('Rococo' . $i);
            $pictures->setPatrimony($patrimoine);
            $manager->persist($pictures);
        }

        $manager->flush();
    }
}
