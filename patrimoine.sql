-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : mar. 04 jan. 2022 à 10:09
-- Version du serveur : 10.4.21-MariaDB
-- Version de PHP : 8.0.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `patrimoine`
--

-- --------------------------------------------------------

--
-- Structure de la table `comment`
--

CREATE TABLE `comment` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `patrimony_id` int(11) DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `comment`
--

INSERT INTO `comment` (`id`, `user_id`, `patrimony_id`, `content`, `created_at`) VALUES
(11, 18, 20, 'superbe magnifique 0', '2021-12-28 11:46:46'),
(12, 18, 20, 'superbe magnifique 1', '2021-12-28 11:46:46'),
(13, 18, 20, 'superbe magnifique 2', '2021-12-28 11:46:46'),
(14, 18, 20, 'superbe magnifique 3', '2021-12-28 11:46:46'),
(15, 18, 20, 'superbe magnifique 4', '2021-12-28 11:46:46'),
(16, 18, 20, 'superbe magnifique 5', '2021-12-28 11:46:46'),
(17, 18, 20, 'superbe magnifique 6', '2021-12-28 11:46:46'),
(18, 18, 20, 'superbe magnifique 7', '2021-12-28 11:46:46'),
(19, 18, 20, 'superbe magnifique 8', '2021-12-28 11:46:46'),
(20, 18, 20, 'superbe magnifique 9', '2021-12-28 11:46:46'),
(21, NULL, 15, 'bouh', '2021-12-28 17:07:56'),
(22, NULL, 15, 'bouh', '2021-12-28 17:08:29'),
(23, NULL, 15, 'ykgyk', '2021-12-28 19:41:34'),
(24, NULL, 15, 'po', '2021-12-28 19:42:17'),
(25, NULL, 14, 'bouoooo', '2021-12-29 20:45:44'),
(26, NULL, 20, 'JDFG', '2022-01-02 14:50:56'),
(27, NULL, 12, 'bok', '2022-01-02 20:27:42');

-- --------------------------------------------------------

--
-- Structure de la table `maintenance`
--

CREATE TABLE `maintenance` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `periodicity` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `maintenance`
--

INSERT INTO `maintenance` (`id`, `name`, `description`, `periodicity`) VALUES
(11, 'maintenance0', 'Divers0', 'mensuel'),
(12, 'maintenance1', 'Divers1', '6mensuel'),
(13, 'maintenance2', 'Divers2', '12mensuel'),
(14, 'maintenance3', 'Divers3', '24mensuel'),
(15, 'maintenance4', 'Divers4', '48mensuel');

-- --------------------------------------------------------

--
-- Structure de la table `patrimony`
--

CREATE TABLE `patrimony` (
  `id` int(11) NOT NULL,
  `maintenance_id` int(11) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `works` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `thumbnail` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `patrimony`
--

INSERT INTO `patrimony` (`id`, `maintenance_id`, `type_id`, `name`, `description`, `works`, `created_at`, `thumbnail`) VALUES
(11, 13, 15, 'fontaine 0', 'merveilleux0', 'beaucoup0', '2021-12-28 11:46:00', 'DSC01702-61d1d1e92270b.jpg'),
(12, 11, 15, 'fontaine 1', 'merveilleux1', 'beaucoup1', '2017-12-28 11:46:00', 'chapelle-de-rochegude-61d40a926ce8d.jpg'),
(13, 15, 15, 'fontaine 2', 'merveilleux2', 'beaucoup2', '2021-12-28 11:46:00', 'DSC01751-61d1fad16a5e3.jpg'),
(14, 15, 15, 'fontaine 3', 'merveilleux3', 'beaucoup3', '2021-12-28 11:46:00', 'DSC01713-61c5907fab433-61ccb6d184ee9.jpg'),
(15, 14, 15, 'fontaine 4', 'merveilleux4', 'beaucoup4', '2021-12-28 11:46:00', 'hellboy02-618bcca6c0626-61d1ae23dfeae.jpg'),
(16, 13, 15, 'fontaine 5', 'merveilleux5', 'beaucoup5', '2021-12-28 11:46:00', 'matinRoutine-618bae31bc89c-61d17e82915d2.png'),
(17, 15, 15, 'fontaine 6', 'merveilleux6', 'beaucoup6', '2021-12-28 11:46:46', 'vignette6'),
(18, 15, 15, 'fontaine 7', 'merveilleux7', 'beaucoup7', '2021-12-28 11:46:46', 'vignette7'),
(19, 15, 15, 'fontaine 8', 'merveilleux8', 'beaucoup8', '2021-12-28 11:46:46', 'vignette8'),
(20, 15, 13, 'fontaine 9', 'merveilleux9', 'beaucoup9', '2021-12-28 11:46:00', 'croix-eglise-saint-privat-d-allier-uxga-61d1d271e5990.jpg'),
(21, 12, 12, 'popaul', 'gros et gras', 'liftinf \r\ngrand nettoyage', '2021-06-30 18:31:00', 'hellboy04-618bcc14d769b-61d40a096095e.jpg'),
(22, 11, 12, 'lavoire', 'QYY', 'QERHQDRHQ', '2022-01-02 14:54:00', 'croix-eglise-saint-privat-d-allier-uxga-61d1d288e9837.jpg'),
(23, 11, 12, 'popaul', 'grand', 'trop', '2022-01-04 09:23:00', 'image-61d40451712a7.webp');

-- --------------------------------------------------------

--
-- Structure de la table `pictures`
--

CREATE TABLE `pictures` (
  `id` int(11) NOT NULL,
  `patrimony_id` int(11) DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `legend` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `pictures`
--

INSERT INTO `pictures` (`id`, `patrimony_id`, `photo`, `legend`) VALUES
(1, 20, 'photo0', 'Rococo0'),
(2, 20, 'photo1', 'Rococo1'),
(3, 20, 'photo2', 'Rococo2'),
(4, 20, 'photo3', 'Rococo3'),
(5, 20, 'photo4', 'Rococo4'),
(6, 20, 'photo5', 'Rococo5'),
(7, 20, 'photo6', 'Rococo6'),
(8, 20, 'photo7', 'Rococo7'),
(9, 20, 'photo8', 'Rococo8'),
(10, 20, 'photo9', 'Rococo9'),
(11, 15, 'dh', 'gh'),
(12, 14, 'hellboy04-618bcc14d769b-61ccc2f90003d.jpg', 'bouh'),
(13, 14, 'matinRoutine-618bae31bc89c-61ccc8a6dfde5.png', 'better'),
(14, 20, 'chapelle-de-rochegude-61cf30027cf74.jpg', 'hbjlbjl'),
(15, 20, 'image-6192a396c5867-61d1adb40a5f0.webp', NULL),
(16, 12, 'hellboy04-6192358b9bdf2-61d1fce3925ea.png', 'better');

-- --------------------------------------------------------

--
-- Structure de la table `types`
--

CREATE TABLE `types` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `types`
--

INSERT INTO `types` (`id`, `name`, `description`) VALUES
(11, 'type0', 'Divers0'),
(12, 'type1', 'Divers1'),
(13, 'type2', 'Divers2'),
(14, 'type3', 'Divers3'),
(15, 'type4', 'Divers4');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `nickname` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:json)',
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `nickname`, `roles`, `password`, `email`) VALUES
(13, 'user0', '[\"ROLE_USER\"]', '$2y$13$CR.i02g2HF/1wQ3SBJusVeyPInIrgOFAnIZMkKpzqwlELaai/yQTS', '81az0@user.com'),
(14, 'user1', '[\"ROLE_USER\"]', '$2y$13$nb7mS3Kd7AFZbgOIo42vQ.ADR4b.zpu0I1Exa9mwMoFMK1yGdfcXO', '56az1@user.com'),
(15, 'user2', '[\"ROLE_USER\"]', '$2y$13$S7qg9lxMBaGtr/ZLSQseFeclgHWqdCMKgVWk3M72FiGE32LUC5hAW', '80az2@user.com'),
(16, 'user3', '[\"ROLE_USER\"]', '$2y$13$dpT/GXotjCq8Bm2fxdkdLeoG6jqsicZExvC/YPr53z8jzDs8mhIvW', '64az3@user.com'),
(17, 'user4', '[\"ROLE_USER\"]', '$2y$13$uTFeJizlbuQyuRW6URQiauXrZIwyFC7Q1TYE68PU/MbfL2mvdOfC2', '69az4@user.com'),
(18, 'admin5', '[\"ROLE_ADMIN\"]', '$2y$13$U5qIV6yQr0NVS1zPHE52ReQ9ufluRBPuXavO5rotb2ELdBRl6AiHq', '93az5@admin.com'),
(19, 'sergio', '[]', '$2y$13$jwL2IWL.LAfwb3LFBAyTMeVhvND1YElTgKs/XBc4Xo.QbaNWNZGl.', 's@t.fr');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_9474526CA76ED395` (`user_id`),
  ADD KEY `IDX_9474526C9A60D1F0` (`patrimony_id`);

--
-- Index pour la table `maintenance`
--
ALTER TABLE `maintenance`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `patrimony`
--
ALTER TABLE `patrimony`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_986416E2F6C202BC` (`maintenance_id`),
  ADD KEY `IDX_986416E2C54C8C93` (`type_id`);

--
-- Index pour la table `pictures`
--
ALTER TABLE `pictures`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_8F7C2FC09A60D1F0` (`patrimony_id`);

--
-- Index pour la table `types`
--
ALTER TABLE `types`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_8D93D649A188FE64` (`nickname`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT pour la table `maintenance`
--
ALTER TABLE `maintenance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT pour la table `patrimony`
--
ALTER TABLE `patrimony`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT pour la table `pictures`
--
ALTER TABLE `pictures`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT pour la table `types`
--
ALTER TABLE `types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `comment`
--
ALTER TABLE `comment`
  ADD CONSTRAINT `FK_9474526C9A60D1F0` FOREIGN KEY (`patrimony_id`) REFERENCES `patrimony` (`id`),
  ADD CONSTRAINT `FK_9474526CA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `patrimony`
--
ALTER TABLE `patrimony`
  ADD CONSTRAINT `FK_986416E2C54C8C93` FOREIGN KEY (`type_id`) REFERENCES `types` (`id`),
  ADD CONSTRAINT `FK_986416E2F6C202BC` FOREIGN KEY (`maintenance_id`) REFERENCES `maintenance` (`id`);

--
-- Contraintes pour la table `pictures`
--
ALTER TABLE `pictures`
  ADD CONSTRAINT `FK_8F7C2FC09A60D1F0` FOREIGN KEY (`patrimony_id`) REFERENCES `patrimony` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
