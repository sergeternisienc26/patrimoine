/*=========================
    bouton recherche
==========================*/

const cards = document.querySelectorAll(" .patrimony_card, .table_patrimony ");

function liveSearch() {
  const search_query = document.getElementById("searchbox").value;

  //utilise innerText(contenu textuel visiblement rendu->ne rend pas les éléments cachés) si contenu est visible
  //utilise textContent(contenu textuel tous les éléments du noeud) pour inclure éléments cachés
  for (let i = 0; i < cards.length; i++) {
    if (
      cards[i].textContent.toLowerCase().includes(search_query.toLowerCase())
    ) {
      cards[i].classList.remove("is-hidden");
    } else {
      cards[i].classList.add("is-hidden");
    }
  }
}

//petit délai "de retard"
let typingTimer;
const typeInterval = 500;

if(document.getElementById("searchbox")) {
  const searchInput = document.getElementById("searchbox");

  searchInput.addEventListener("keyup", () => {
    clearTimeout(typingTimer);
    typingTimer = setTimeout(liveSearch, typeInterval);
  });
}


