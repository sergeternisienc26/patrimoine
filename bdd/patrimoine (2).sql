-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : mar. 12 avr. 2022 à 15:41
-- Version du serveur :  10.4.19-MariaDB
-- Version de PHP : 8.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `patrimoine`
--

-- --------------------------------------------------------

--
-- Structure de la table `comment`
--

CREATE TABLE `comment` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `patrimony_id` int(11) DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `maintenance`
--

CREATE TABLE `maintenance` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `periodicity` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `maintenance`
--

INSERT INTO `maintenance` (`id`, `name`, `description`, `periodicity`) VALUES
(16, 'maintenance01', 'Divers01', 'mensuel'),
(17, 'maintenance02', 'Divers02', '6mensuel'),
(18, 'maintenance03', 'Divers03', '12mensuel'),
(19, 'maintenance04', 'Divers04', '24mensuel'),
(20, 'maintenance05', 'Divers05', '48mensuel');

-- --------------------------------------------------------

--
-- Structure de la table `patrimony`
--

CREATE TABLE `patrimony` (
  `id` int(11) NOT NULL,
  `maintenance_id` int(11) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `works` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `thumbnail` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lat` double DEFAULT NULL,
  `lon` double DEFAULT NULL,
  `date_maintenance` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `patrimony`
--

INSERT INTO `patrimony` (`id`, `maintenance_id`, `type_id`, `name`, `description`, `works`, `created_at`, `thumbnail`, `lat`, `lon`, `date_maintenance`) VALUES
(51, 20, 16, 'Maison Serge', 'Maison idéale pour chambres d\'hôtes', 'RDC à aménager', '2017-04-12 15:19:00', 'combriaux-62557e384df0d.jpg', 44.990290031305, 3.6807096004486, NULL),
(52, 18, 16, 'lavoir', 'lavoir isolé', 'peu', '2021-04-12 15:27:00', 'cormes-lavoir-restauration8-2017-62557eba36a53.jpg', 44.998905949095, 3.6929742228778, NULL),
(55, 18, 17, 'les trois croix', 'panorama', 'peu', '2022-04-12 15:36:00', 'troiscroix-6255808d6bd27.jpg', 45.01175996269, 3.6639817799635, NULL),
(56, 17, 18, 'église saint Privat d\'allier', 'église romane', 'trop', '2022-04-12 15:37:00', '188048-english-roman-church-saint-privat-dallier-6255811e33963.jpg', 44.990098203981, 3.6774641078081, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `pictures`
--

CREATE TABLE `pictures` (
  `id` int(11) NOT NULL,
  `patrimony_id` int(11) DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `legend` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `pictures`
--

INSERT INTO `pictures` (`id`, `patrimony_id`, `photo`, `legend`) VALUES
(33, NULL, 'C:\\xampp\\tmp\\php9AA5.tmp', 'nxn');

-- --------------------------------------------------------

--
-- Structure de la table `types`
--

CREATE TABLE `types` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `types`
--

INSERT INTO `types` (`id`, `name`, `description`) VALUES
(16, 'Architectural', 'architectural'),
(17, 'Paysager', 'paysage chemins'),
(18, 'Culturel', 'culturel');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `nickname` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:json)',
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `nickname`, `roles`, `password`, `email`) VALUES
(25, 'admin5', '[\"ROLE_ADMIN\"]', '$2y$13$C02wgHYcJXF.N3qJRmyC9OorwtXE78NDNMwkEjWCm9x/hiMWzP9j2', '76az5@admin.com'),
(29, 'sergui', '[\"ROLE_ADMIN\"]', '$2y$13$O7Nbak.mMEgLjBPXvwuAPOCGUd.wImgTkdGZOCuWJAgV7.CSgPlCy', 's@t.fr'),
(30, 'boubou', '[\"ROLE_USER\"]', '$2y$13$tNsX1FslZwu7.9MD.6pJv.wLXULQXoQFWIGYSuBHg13/JMH3RmFyC', 's@t.fr');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_9474526CA76ED395` (`user_id`),
  ADD KEY `IDX_9474526C9A60D1F0` (`patrimony_id`);

--
-- Index pour la table `maintenance`
--
ALTER TABLE `maintenance`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `patrimony`
--
ALTER TABLE `patrimony`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_986416E2F6C202BC` (`maintenance_id`),
  ADD KEY `IDX_986416E2C54C8C93` (`type_id`);

--
-- Index pour la table `pictures`
--
ALTER TABLE `pictures`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_8F7C2FC09A60D1F0` (`patrimony_id`);

--
-- Index pour la table `types`
--
ALTER TABLE `types`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_8D93D649A188FE64` (`nickname`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT pour la table `maintenance`
--
ALTER TABLE `maintenance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT pour la table `patrimony`
--
ALTER TABLE `patrimony`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT pour la table `pictures`
--
ALTER TABLE `pictures`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT pour la table `types`
--
ALTER TABLE `types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `comment`
--
ALTER TABLE `comment`
  ADD CONSTRAINT `FK_9474526C9A60D1F0` FOREIGN KEY (`patrimony_id`) REFERENCES `patrimony` (`id`),
  ADD CONSTRAINT `FK_9474526CA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `patrimony`
--
ALTER TABLE `patrimony`
  ADD CONSTRAINT `FK_986416E2C54C8C93` FOREIGN KEY (`type_id`) REFERENCES `types` (`id`),
  ADD CONSTRAINT `FK_986416E2F6C202BC` FOREIGN KEY (`maintenance_id`) REFERENCES `maintenance` (`id`);

--
-- Contraintes pour la table `pictures`
--
ALTER TABLE `pictures`
  ADD CONSTRAINT `FK_8F7C2FC09A60D1F0` FOREIGN KEY (`patrimony_id`) REFERENCES `patrimony` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
