-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : jeu. 07 avr. 2022 à 13:28
-- Version du serveur :  10.4.19-MariaDB
-- Version de PHP : 8.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `patrimoine`
--

-- --------------------------------------------------------

--
-- Structure de la table `comment`
--

CREATE TABLE `comment` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `patrimony_id` int(11) DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `comment`
--

INSERT INTO `comment` (`id`, `user_id`, `patrimony_id`, `content`, `created_at`, `active`) VALUES
(29, 25, 28, 'superbe magnifique 1', '2022-03-21 08:29:22', 1),
(30, 25, 28, 'superbe magnifique 2', '2022-03-21 08:29:22', 1),
(31, 25, 28, 'superbe magnifique 3', '2022-03-21 08:29:22', 1),
(32, 25, 28, 'superbe magnifique 4', '2022-03-21 08:29:22', 1),
(33, 25, 28, 'superbe magnifique 5', '2022-03-21 08:29:22', 1),
(34, 25, 28, 'superbe magnifique 6', '2022-03-21 08:29:22', 1),
(35, 25, 28, 'superbe magnifique 7', '2022-03-21 08:29:22', 1),
(37, 25, 28, 'superbe magnifique 9', '2022-03-21 08:29:22', 1),
(54, NULL, 25, 'tout va bien', '2022-03-28 14:48:29', 1),
(59, NULL, 45, 'h,gh', '2022-04-05 15:13:07', 1);

-- --------------------------------------------------------

--
-- Structure de la table `maintenance`
--

CREATE TABLE `maintenance` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `periodicity` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `maintenance`
--

INSERT INTO `maintenance` (`id`, `name`, `description`, `periodicity`) VALUES
(16, 'maintenance01', 'Divers01', 'mensuel'),
(17, 'maintenance02', 'Divers02', '6mensuel'),
(18, 'maintenance03', 'Divers03', '12mensuel'),
(19, 'maintenance04', 'Divers04', '24mensuel'),
(20, 'maintenance05', 'Divers05', '48mensuel');

-- --------------------------------------------------------

--
-- Structure de la table `patrimony`
--

CREATE TABLE `patrimony` (
  `id` int(11) NOT NULL,
  `maintenance_id` int(11) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `works` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `thumbnail` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lat` double DEFAULT NULL,
  `lon` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `patrimony`
--

INSERT INTO `patrimony` (`id`, `maintenance_id`, `type_id`, `name`, `description`, `works`, `created_at`, `thumbnail`, `lat`, `lon`) VALUES
(25, 17, 17, 'fontaine 1', 'merveilleux1', 'beaucoup1', '2018-03-21 08:29:00', 'hellboy03-6239e557e428b.png', 45.010751, 3.662095),
(27, 20, 17, 'fontaine 3', 'merveilleux3', 'beaucoup3', '2022-03-21 08:29:00', 'photo-1512101176959-c557f3516787-6239e0e7d9484.jpg', 45.023864, 3.685736),
(28, 18, 17, 'fontaine 4', 'merveilleux4', 'beaucoup4', '2021-03-21 08:29:00', 'matinForme-623b37b21432b.jpg', 45.00879, 3.693498),
(45, 16, 17, 'morilles', 'plein de morilles', 'manger et cuire', '2022-04-05 11:30:00', 'images-624c0c71c397c.jpg', 45.014816585928, 3.6761283874512),
(50, 20, 18, 'khyou', 'A éviter', 'TROP', '2022-04-07 12:27:00', '6QVKJMVMZVGQ5PICNY3HZU752A-624ebcb80d631.jpg', 44.990465259907, 3.6808061599731);

-- --------------------------------------------------------

--
-- Structure de la table `pictures`
--

CREATE TABLE `pictures` (
  `id` int(11) NOT NULL,
  `patrimony_id` int(11) DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `legend` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `pictures`
--

INSERT INTO `pictures` (`id`, `patrimony_id`, `photo`, `legend`) VALUES
(19, 28, 'photo2', 'Rococo2'),
(21, 28, 'photo4', 'Rococo4'),
(22, 28, 'photo5', 'Rococo5'),
(23, 28, 'photo6', 'Rococo6'),
(24, 28, 'photo7', 'Rococo7'),
(25, 28, 'photo8', 'Rococo8'),
(26, 28, 'photo9', 'Rococo9'),
(33, NULL, 'C:\\xampp\\tmp\\php9AA5.tmp', 'nxn'),
(34, 25, 'hellboy03-623aca8f22019.jpg', 'pmhpmmp'),
(45, 27, 'cormes-lavoir-restauration8-2017-624154f33f7c6.jpg', 'èlmri');

-- --------------------------------------------------------

--
-- Structure de la table `types`
--

CREATE TABLE `types` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `types`
--

INSERT INTO `types` (`id`, `name`, `description`) VALUES
(16, 'Architectural', 'architectural'),
(17, 'Paysager', 'paysage chemins'),
(18, 'Culturel', 'culturel');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `nickname` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:json)',
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `nickname`, `roles`, `password`, `email`) VALUES
(25, 'admin5', '[\"ROLE_ADMIN\"]', '$2y$13$C02wgHYcJXF.N3qJRmyC9OorwtXE78NDNMwkEjWCm9x/hiMWzP9j2', '76az5@admin.com'),
(29, 'sergui', '[\"ROLE_ADMIN\"]', '$2y$13$O7Nbak.mMEgLjBPXvwuAPOCGUd.wImgTkdGZOCuWJAgV7.CSgPlCy', 's@t.fr'),
(30, 'boubou', '[\"ROLE_USER\"]', '$2y$13$tNsX1FslZwu7.9MD.6pJv.wLXULQXoQFWIGYSuBHg13/JMH3RmFyC', 's@t.fr');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_9474526CA76ED395` (`user_id`),
  ADD KEY `IDX_9474526C9A60D1F0` (`patrimony_id`);

--
-- Index pour la table `maintenance`
--
ALTER TABLE `maintenance`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `patrimony`
--
ALTER TABLE `patrimony`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_986416E2F6C202BC` (`maintenance_id`),
  ADD KEY `IDX_986416E2C54C8C93` (`type_id`);

--
-- Index pour la table `pictures`
--
ALTER TABLE `pictures`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_8F7C2FC09A60D1F0` (`patrimony_id`);

--
-- Index pour la table `types`
--
ALTER TABLE `types`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_8D93D649A188FE64` (`nickname`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT pour la table `maintenance`
--
ALTER TABLE `maintenance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT pour la table `patrimony`
--
ALTER TABLE `patrimony`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT pour la table `pictures`
--
ALTER TABLE `pictures`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT pour la table `types`
--
ALTER TABLE `types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `comment`
--
ALTER TABLE `comment`
  ADD CONSTRAINT `FK_9474526C9A60D1F0` FOREIGN KEY (`patrimony_id`) REFERENCES `patrimony` (`id`),
  ADD CONSTRAINT `FK_9474526CA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `patrimony`
--
ALTER TABLE `patrimony`
  ADD CONSTRAINT `FK_986416E2C54C8C93` FOREIGN KEY (`type_id`) REFERENCES `types` (`id`),
  ADD CONSTRAINT `FK_986416E2F6C202BC` FOREIGN KEY (`maintenance_id`) REFERENCES `maintenance` (`id`);

--
-- Contraintes pour la table `pictures`
--
ALTER TABLE `pictures`
  ADD CONSTRAINT `FK_8F7C2FC09A60D1F0` FOREIGN KEY (`patrimony_id`) REFERENCES `patrimony` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
